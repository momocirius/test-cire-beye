<?php

interface Repository
{
    public function getById($id);

    public function getTextByAttribute($attribute);
}
