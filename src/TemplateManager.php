<?php

class TemplateManager
{
    public function getTemplateComputed(Template $tpl, array $data)
    {
        if (!$tpl) {
            throw new RuntimeException('no tpl given');
        }

        $replaced = clone($tpl);
        $replaced->subject = $this->computeText($replaced->subject, $data);
        $replaced->content = $this->computeText($replaced->content, $data);

        return $replaced;
    }

    private function computeText($text, array $data)
    {
        $APPLICATION_CONTEXT = ApplicationContext::getInstance();

        $lesson = (isset($data['lesson']) and $data['lesson'] instanceof Lesson) ? $data['lesson'] : null;

        /*
         * Table of different element we need to replace in the text
         * If one day you want to add more replaceable variable a to the text, you just have to add the variable name
         * '[variable_name]' and the value 'value' to this array.
         * */
        $textDetailsToEdit = [];

        if ($lesson) {
            $_lessonFromRepository = LessonRepository::getInstance()->getById($lesson->id);
            $usefulObject = MeetingPointRepository::getInstance()->getById($lesson->meetingPointId);

            if (strpos($text, '[lesson:instructor_link]')) {
                $instructor = InstructorRepository::getInstance()->getById($lesson->instructorId);
            }


            $textDetailsToEdit['[lesson:summary_html]'] = Lesson::renderHtml($_lessonFromRepository);
            $textDetailsToEdit['[lesson:summary]'] = Lesson::renderText($_lessonFromRepository);
            $textDetailsToEdit['[lesson:instructor_name]'] = InstructorRepository::getInstance()->getById($lesson->instructorId)->firstname;

        }

        /*
         *
         * */
        $textDetailsToEdit['[lesson:meeting_point]'] = $lesson->meetingPointId ? $usefulObject->name : '(Undefined)';

        $textDetailsToEdit['[lesson:start_date]'] = $lesson->start_time->format('d/m/Y');
        $textDetailsToEdit['[lesson:start_time]'] = $lesson->start_time->format('H:i');
        $textDetailsToEdit['[lesson:end_time]'] = $lesson->end_time->format('H:i');
        $textDetailsToEdit['[lesson:link]'] = isset($instructor) ? $usefulObject->url . '/' . $instructor->id . '/lesson/' . $_lessonFromRepository->id : '';

        /*
         * USER
         * [user:*]
         */
        $_user = ((isset($data['user']) and $data['user'] instanceof Learner)) ? $data['user'] : $APPLICATION_CONTEXT->getCurrentUser();
        if ($_user) {
            $textDetailsToEdit['[user:first_name]'] = ucfirst(mb_strtolower($_user->firstname));
        }

        return str_replace(array_keys($textDetailsToEdit), array_values($textDetailsToEdit), $text);
    }

}
